<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Student;
use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\StudentCreateRequest;
use App\Http\Requests\StudentUpdateRequest;
use App\Repositories\StudentRepository;
use App\Validators\StudentValidator;


class StudentsController extends Controller
{

    /**
     * @var StudentRepository
     */
    protected $repository;

    /**
     * @var StudentValidator
     */
    protected $validator;

    // Injecting Repository an validater to Controller
    public function __construct(StudentRepository $repository, StudentValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // get all student using repository all() function
        $students = $this->repository->all();

            return response()->json([
                'data' => $students,
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StudentCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            // using repository create function and validation class to add new student

            // validate user
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            // create user
            $student = $this->repository->create($request->all());

            $response = [
                'message' => 'Student created.',
                'data'    => $student->toArray(),
            ];

                return response()->json($response);

        } catch (ValidatorException $e) {

               return response()->json(['error' => $e->getMessageBag()], 403);
            
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get Students by class Id using repository find method
        $class = Student::where('class_id', '=', $id)->get();

        if ($class->first()) {
            return response()->json([
                'data' => $class,
            ]);
        }
        return response()->json([
                    'error'   => true,
                    'message' => "No Student in Class"
                ]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  StudentUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(StudentUpdateRequest $request, $id)
    {

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

}
