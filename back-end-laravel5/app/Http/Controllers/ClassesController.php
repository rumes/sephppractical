<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\ClassesCreateRequest;
use App\Http\Requests\ClassesUpdateRequest;
use App\Repositories\ClassesRepository;
use App\Validators\ClassesValidator;
use App\Entities\Student;


class ClassesController extends Controller
{

    /**
     * @var ClassesRepository
     */
    protected $repository;

    /**
     * @var ClassesValidator
     */
    protected $validator;

    // Injecting Repository an validater to Controller
    public function __construct(ClassesRepository $repository, ClassesValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all class using repository all() function
        $classes = $this->repository->all();


            return response()->json([
                'data' => $classes,
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ClassesCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            // using repository create function and validation class to add new class

            // validate class
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $class = $this->repository->create($request->all());

            $response = [
                'message' => 'Classes created.',
                'data'    => $class->toArray(),
            ];

                return response()->json($response);
            

        } catch (ValidatorException $e) {
                return response()->json(['error' => $e->getMessageBag()], 403);

        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // search for the class that have $id and return as json
       $classes = $this->repository->find($id);
            if ($classes->count() > 0) {
               return response()->json([
                'data' => $classes,
            ]);
            }else{
            // if  no record maching $id send  error
                return response()->json(['error' => "No Classes in this Grad"], 403);
            }

            
        
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  ClassesUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(ClassesUpdateRequest $request, $id)
    {

       
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete class
        $deleted = $this->repository->delete($id);

            return response()->json([
                'message' => 'Classes deleted.',
                'deleted' => $deleted,
            ]);
        
    }


}
