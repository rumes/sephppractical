<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGradIdClassIdInStudent extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('students', function(Blueprint $table)
		{
			$table-> unsignedInteger ('grad_id')->after('email');
			$table-> unsignedInteger ('class_id')->after('grad_id');
            $table->foreign('grad_id')
            ->references('id')->on('grads')
            ->onDelete('cascade');
            $table->foreign('class_id')
            ->references('id')->on('classes')
            ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('students', function(Blueprint $table)
		{
			$table->dropColumn('grad_id');
			$table->dropColumn('class_id');
		});
	}

}
