<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Classes extends Model implements Transformable
{
    use TransformableTrait;
    protected $table = 'classes';

    protected $fillable = ['name','grad_id'];

}
