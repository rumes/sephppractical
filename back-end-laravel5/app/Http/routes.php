<?php
Route::get('api/grads-and-class', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

// routing for grads calls
Route::resource('api/grads', 'GradsController');

// routing for classes calls
Route::resource('api/classes','ClassesController');

// routing for student calls
Route::resource('api/students','StudentsController');

Route::group(['prefix' => 'api'], function()
{
    Route::resource('authenticate', 'AuthenticateController', ['only' => ['index']]);
    Route::post('authenticate', 'AuthenticateController@authenticate');
});


Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
