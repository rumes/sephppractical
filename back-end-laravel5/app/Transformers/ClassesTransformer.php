<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Classes;

/**
 * Class ClassesTransformer
 * @package namespace App\Transformers;
 */
class ClassesTransformer extends TransformerAbstract
{

    /**
     * Transform the \Classes entity
     * @param \Classes $model
     *
     * @return array
     */
    public function transform(Classes $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
