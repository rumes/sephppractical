#School System REST API 

###Author: J.K.R.S. Kumarasinghe 
###Tecnologies:Laravel 5.0.16, Angular 2, MySQL

##1.Implementation
##Backend

- Clone the git Repository
- In terminal go to the project folder and move in to /back-end-laravel 
- Execute `"composer install"`
- Go into .env and set dabase variable
- Go into /config/database.php and set dabase variable
- Creat Database named se_php_practicle
- importing se_php_practicle.sql file or doing "artisan migration" craeat the requierd tables
- Move in to /public folder
- Run the backend by using `"php -S localhost:8000"` on port 8000 in local host. you can use any other way but backend must run on port 8000

 
##Frontend

- Install nodejs and npm(node package manager)
- go in to /front-end-angular2 folder
- run "npm install"
- run "npm start"

>Now you are good to go

##2.Usage
###Login
- You have to login before use the application

>User Name : chris@scotch.io , Password : secret

###Home page
- In Home page all grads are listed and end of the every grade row have `+` button and clicking it you can see the all classes under that grad
- In header on the left top corner has the menu button and clicking that you can open the menu drawer in left side.
- in right top corner have the logout link and clicking it you are redirect to the login page.

###Add Grads Page
- Click on the menu button and Click on the Add Grad Link
- In Add Grads Page all the Grads are List down on a data table
- In Right bottom corner have the "Add Grade" button and clicking that will pop up a dialog box to add Grad.
- In Gradd add dialog box it has a input text to give the new Grad name and clicking save you can add the grad.
- If the Name is already taken it will give the error
- If Grad is added succesfuly It give the succes msg and reset the data table
- In this page clicking on a grad row it will re direct you to the class table that own by that grad and from their you can add Classes to the parent grad or delete class from that grad.

###Add Classes page
- Click on the menu button and Click on the Add Class Link
- In this Page Has form to add class after filling the form you can click the add button.
- If this class is already in the selected grad it show error
- If class added successfuly it re direct to the Class list that have same grad_id
- you can delete classes or create new classes from here
- From that page clicking on a class row you are redirected to that class student List from their you can add Student to that class

###Add Student Page 
- Click on the menu button and Click on the Add Student Link
- In this Page you have to fill the add student form and submit to add a Student.
- If the Studnet validate show the success msg.
- If the new Student not validated give the error msg

###Class List
- Click on the menu button and Click on the Class List Link
- In here all the classes are listed without a order 

###Student List Page 

- Click on the menu button and Click on the Student List Link
- In here all the Student are listed without order

##3.Architectural Decisions

###DB
Used MySQL as Db and Use 4 table to In the Db

>User Table
- id- primary key
- name 
- email - this is uniq and use as username
- password 
- remember token
- created at
- updated at

> Grads Table
- id - primay key
- name - unique

>Classes Table
- id - primary key
- name - String
- grad_id - foring key(grads.id)

in this table  combination of name and grad_id use as composite unique key

>Student Table
- id - primary key
- name - 
- email - Unique key
- class_id - foring key(classes.id)
- grad_id - foring key(grads.id * optional because using class_id we can find th grad id) 

When Creating the Db I assumed 
`grad has many classes and classes has only one class`
`Class has many Student and Student has one Class`

### Back-end (Laravel)
- When desining the backend I use Token base authentication that front end send the credential and backend frovided with a accesstoken.
- Use Repository pattern that Creat a Repositrory class with collection of common action that use by Controller and Inject that repository to Controller using constructor. Use third party library to genarate repository
- Uses laravel inbuild validation for validat requierd, unique, email and desing custom validator to validate composite Key.
- Use json to return the data from backend.
- Use Route::resource to define end poins

### Front End (Angular 2)
- Use Services to handle back-end calls and authentication.
- Use local storage to store token and for authentication
- Primeng used for styling.
- angular 2 build in validator use to client side validation.
- Custom Build side drawer and scroll-header-panel using angular 2 animation.

>Use Repository pattern because it is suport for scalability and I need to learn that pattern.

>Use Services to maximse the code re use.

> Testing: I do not have much of experince in laravel testing. Used PostMan to test  backend calls.


