<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class ClassesValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
        	'name' => 'required|unique_with:classes,grad_id',
        	'grad_id' => 'required|exists:grads,id'
        ],
        // ValidatorInterface::RULE_UPDATE => [],
   ];
}
