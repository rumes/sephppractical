<?php

namespace App\Presenters;

use App\Transformers\ClassesTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ClassesPresenter
 *
 * @package namespace App\Presenters;
 */
class ClassesPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ClassesTransformer();
    }
}
