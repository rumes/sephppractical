<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Entities\Classes;
use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\GradsCreateRequest;
use App\Http\Requests\GradsUpdateRequest;
use App\Repositories\GradsRepository;
use App\Validators\GradsValidator;


class GradsController extends Controller
{

    /**
     * @var GradsRepository
     */
    protected $repository;

    /**
     * @var GradsValidator
     */
    protected $validator;

    // Injecting Repository an validater to Controller
    public function __construct(GradsRepository $repository, GradsValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Use repository all() function to get all grads
        $grads = $this->repository->all();


            return response()->json([
                'data' => $grads,
            ]);
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  GradsCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            // using Class repository and Class validator to  Create Grads
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $grad = $this->repository->create($request->all());

            $response = [
                'message' => 'Grads created.',
                'data'    => $grad->toArray(),
            ];

                return response()->json($response);
        } catch (ValidatorException $e) {
                return response()->json(['error' => $e->getMessageBag()], 403);

        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // using find function in repository get grad by id
        $classes = Classes::where('grad_id', '=' , $id)->get();


            return response()->json([
                'data' => $classes,
            ]);
       
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  GradsUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(GradsUpdateRequest $request, $id)
    {

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
