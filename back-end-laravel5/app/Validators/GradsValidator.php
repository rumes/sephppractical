<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class GradsValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => ['name' => 'required|unique:grads'],
   ];
}
