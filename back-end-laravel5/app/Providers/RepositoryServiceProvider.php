<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(\App\Repositories\GradsRepository::class, \App\Repositories\GradsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ClassesRepository::class, \App\Repositories\ClassesRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\StudentRepository::class, \App\Repositories\StudentRepositoryEloquent::class);
        //:end-bindings:
	}

}
