<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface GradsRepository
 * @package namespace App\Repositories;
 */
interface GradsRepository extends RepositoryInterface
{
    //
}
