<?php

namespace App\Presenters;

use App\Transformers\GradsTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class GradsPresenter
 *
 * @package namespace App\Presenters;
 */
class GradsPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new GradsTransformer();
    }
}
