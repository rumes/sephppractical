<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class StudentValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
        	'name' => 'required',
        	'email' => 'required|email|unique:students',
        	'grad_id' => 'required|exists:grads,id',
        	'class_id' => 'required|exists:classes,id'
        ],
        // ValidatorInterface::RUL|E_UPDATE => [],
   ];
}
