<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Grads;

/**
 * Class GradsTransformer
 * @package namespace App\Transformers;
 */
class GradsTransformer extends TransformerAbstract
{

    /**
     * Transform the \Grads entity
     * @param \Grads $model
     *
     * @return array
     */
    public function transform(Grads $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
