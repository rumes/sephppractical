<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Grads extends Model implements Transformable
{
    use TransformableTrait;
    protected $table = 'grads';

    protected $fillable = ['id','name','created_at','updated_at'];

}
