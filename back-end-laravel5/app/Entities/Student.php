<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Student extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['name','email','grad_id','class_id'];

}
